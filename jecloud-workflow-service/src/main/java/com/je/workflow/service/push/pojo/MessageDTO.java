/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push.pojo;

import com.je.bpm.engine.upcoming.UpcomingDTO;
import com.je.common.base.util.SecurityUserHolder;

import java.util.HashMap;
import java.util.Map;

public class MessageDTO {
    String submitType;
    String modelName;
    String comment;
    String pkValue;
    String funcCode;
    String submitUserName;
    String submitUserId;
    Map<String, String> variables;


    public static MessageDTO build(String submitType, String modelName, String comment, String pkValue, String funcCode, String nodeName
            , String submitUserId, String submitUserName, Map<String, Object> bean) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setSubmitType(submitType).setModelName(modelName).setComment(comment)
                .setPkValue(pkValue).setFuncCode(funcCode).setSubmitUserId(submitUserId)
                .setSubmitUserId(submitUserName);
        putVariables(messageDTO, submitType, modelName, comment, nodeName, bean);
        return messageDTO;
    }

    public static MessageDTO build(UpcomingDTO upcomingDTO) {
        MessageDTO messageDTO = new MessageDTO();
        String submitType = upcomingDTO.getUpcomingInfo().getSubmitType().getName();
        String modelName = upcomingDTO.getModelName();
        String comment = upcomingDTO.getUpcomingInfo().getComment();
        String pkValue = upcomingDTO.getUpcomingInfo().getBeanId();
        String funcCode = upcomingDTO.getFuncCode();
        messageDTO.setSubmitType(submitType).setModelName(modelName).setComment(comment)
                .setPkValue(pkValue).setFuncCode(funcCode).setSubmitUserId(SecurityUserHolder.getCurrentAccountRealUserId())
                .setSubmitUserId(SecurityUserHolder.getCurrentAccountRealUserName());
        putVariables(messageDTO, upcomingDTO);
        return messageDTO;
    }

    public static MessageDTO build(String submitType, String modelName, String comment, String pkValue, String funcCode) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setSubmitType(submitType).setModelName(modelName).setComment(comment)
                .setPkValue(pkValue).setFuncCode(funcCode).setSubmitUserId(SecurityUserHolder.getCurrentAccountRealUserId())
                .setSubmitUserId(SecurityUserHolder.getCurrentAccountRealUserName());
        return messageDTO;
    }

    private static void putVariables(MessageDTO messageDTO, UpcomingDTO upcomingDTO) {
        Map<String, String> variables = new HashMap<>();
        variables.put("@SUBMIT_OPERATE@", upcomingDTO.getUpcomingInfo().getSubmitType().getName());
        //流程名称
        variables.put("@PROCESS_NAME@", upcomingDTO.getModelName());
        //审批意见
        variables.put("@SUBMIT_COMMENTS@", upcomingDTO.getUpcomingInfo().getComment());
        //活动节点
        variables.put("@PROCESS_CURRENTTASK@", upcomingDTO.getNodeName());
        Map<String, Object> bean = upcomingDTO.getUpcomingInfo().getBean();
        if (bean != null) {
            for (Map.Entry<String, Object> entry : bean.entrySet()) {
                variables.put(entry.getKey(), entry.getValue() == null ? "" : entry.getValue().toString());
            }
        }
        messageDTO.setVariables(variables);
    }

    private static void putVariables(MessageDTO messageDTO, String submitType, String modelName, String comment, String nodeName
            , Map<String, Object> bean) {
        Map<String, String> variables = new HashMap<>();
        variables.put("@SUBMIT_OPERATE@", submitType);
        //流程名称
        variables.put("@PROCESS_NAME@", modelName);
        //审批意见
        variables.put("@SUBMIT_COMMENTS@", comment);
        //活动节点
        variables.put("@PROCESS_CURRENTTASK@", nodeName);
        if (bean != null) {
            for (Map.Entry<String, Object> entry : bean.entrySet()) {
                variables.put(entry.getKey(), entry.getValue() == null ? "" : entry.getValue().toString());
            }
        }
        messageDTO.setVariables(variables);
    }

    public String getSubmitType() {
        return submitType;
    }

    public MessageDTO setSubmitType(String submitType) {
        this.submitType = submitType;
        return this;
    }

    public String getModelName() {
        return modelName;
    }

    public MessageDTO setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public MessageDTO setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public String getPkValue() {
        return pkValue;
    }

    public MessageDTO setPkValue(String pkValue) {
        this.pkValue = pkValue;
        return this;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public MessageDTO setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public MessageDTO setVariables(Map<String, String> variables) {
        this.variables = variables;
        return this;
    }

    public String getSubmitUserName() {
        return submitUserName;
    }

    public MessageDTO setSubmitUserName(String submitUserName) {
        this.submitUserName = submitUserName;
        return this;
    }

    public String getSubmitUserId() {
        return submitUserId;
    }

    public MessageDTO setSubmitUserId(String submitUserId) {
        this.submitUserId = submitUserId;
        return this;
    }
}
