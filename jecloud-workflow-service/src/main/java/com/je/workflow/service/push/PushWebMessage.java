/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.push;

import cn.hutool.core.date.DateUtil;
import com.google.common.base.Strings;
import com.je.bpm.core.model.config.ProcessRemindTemplateTypeEnum;
import com.je.bpm.core.model.config.process.ProcessRemindTemplate;
import com.je.workflow.service.push.pojo.CommonMessageVo;
import com.je.workflow.service.push.pojo.PushMessageTypeEnum;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "webMessage")
public class PushWebMessage extends AbstractPushMessageService {

    @Override
    public void execute(List<ProcessRemindTemplate> processRemindTemplates, String title, String userName,
                        String modelName, String submitType, String comment, String userId,
                        CommonMessageVo commonMessageVo, String thirdPartyContent, String customerContent) {
        String contentTemplate = "";
        for (ProcessRemindTemplate processRemindTemplate : processRemindTemplates) {
            if (!ProcessRemindTemplateTypeEnum.DWR.getType().equals(processRemindTemplate.getType().getType())) {
                continue;
            }
            contentTemplate = processRemindTemplate.getTemplate();
        }
        if (Strings.isNullOrEmpty(title)) {
            title = "流程提醒";
        } else {
            title = CommonSystemVariable.formatVariable(title);
        }
        String content = "";
        if (Strings.isNullOrEmpty(contentTemplate)) {
            content = String.format("由【%s】在【%s】给您提交了【%s】任务, 执行操作：【%s】,执行意见：【%s】",
                    userName, DateUtil.now(), modelName, submitType, comment);
            if (Strings.isNullOrEmpty(comment)) {
                content = String.format("由【%s】在【%s】给您提交了【%s】任务, 执行操作：【%s】",
                        userName, DateUtil.now(), modelName, submitType);
            }
        } else {
            content = WorkFlowVariable.formatVariable(contentTemplate, commonMessageVo.getVariables());
            content = CommonSystemVariable.formatVariable(content);
        }
        commonMessageVo.getNotice().setTitle(title);
        if (!Strings.isNullOrEmpty(customerContent)) {
            content = customerContent;
        }
        commonMessageVo.getNotice().setContent(content);
        //右下角通知
        RedisDelayQueue redisDelayQueue = new RedisDelayQueue(redisTemplate, delayQueueName);
        redisDelayQueue.setDelayQueue(commonMessageVo,
                PushMessageTypeEnum.SOCKET_PUSH_OPEN_FUNC_FORM_MSG.getName(),
                userId + "_" + title + "_" + commonMessageVo.getNotice().getContent(), System.currentTimeMillis());
    }

}
