/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask;

public enum CurrentUserTaskEnum {
    PI_PREAPPROV("待我审批"),
    PI_APPROVED("已办理"),
    PI_OWNER("我的流程"),
    PI_DELAY("延时处理"),
    PI_INFORMIEREN("审批告知");

    String name;

    public String getName() {
        return name;
    }

    CurrentUserTaskEnum(String name) {
    }

    public static CurrentUserTaskEnum getByType(String type) {
        if (type.equals(PI_PREAPPROV.toString())) {
            return PI_PREAPPROV;
        } else if (type.equals(PI_APPROVED.toString())) {
            return PI_APPROVED;
        } else if (type.equals(PI_OWNER.toString())) {
            return PI_OWNER;
        } else if (type.equals(PI_DELAY.toString())) {
            return PI_DELAY;
        } else if (type.equals(PI_INFORMIEREN.toString())) {
            return PI_INFORMIEREN;
        }
        return null;
    }

}
