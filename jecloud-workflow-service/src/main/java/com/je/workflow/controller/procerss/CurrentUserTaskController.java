/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.controller.procerss;

import com.google.common.base.Strings;
import com.je.bpm.engine.ActivitiException;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.workflow.service.usertask.CurrentUserParam;
import com.je.workflow.service.usertask.CurrentUserTaskEnum;
import com.je.workflow.service.usertask.CurrentUserTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 任务待办
 */

@RestController
@RequestMapping(value = "/je/workflow/currentUserTask")
public class CurrentUserTaskController extends AbstractPlatformController {

    @Autowired
    private CurrentUserTaskService currentUserTaskService;

    /**
     * 获取当前人员待办信息
     */
    @RequestMapping(value = "/getTask", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getTask(BaseMethodArgument param, HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();
        String type = getStringParameter(request, "type");
        CurrentUserTaskEnum findCurrentTaskEnum = CurrentUserTaskEnum.getByType(type);
        if (findCurrentTaskEnum == null) {
            throw new ActivitiException("查询类型参数异常！");
        }
        Page page = new Page<>(param.getPage(), param.getLimit());

        String userName = getStringParameter(request, "userName");
        String startTime = getStringParameter(request, "startTime");
        String endTime = getStringParameter(request, "endTime");
        String end = getStringParameter(request, "end");
        String sort = getStringParameter(request, "sort");
        CurrentUserParam upcomingParam = CurrentUserParam.build(userName, startTime, endTime, end, page, findCurrentTaskEnum, sort);
        Map<String, Long> badges = currentUserTaskService.getBadges(upcomingParam);
        Map<String, Object> list = currentUserTaskService.getCurrentUserTaskList(findCurrentTaskEnum, upcomingParam);
        resultMap.put("data", list);
        resultMap.put("badges", badges);
        return BaseRespResult.successResult(resultMap);
    }

    /**
     * 获取流程快捷待办list
     */
    @RequestMapping(value = "/getInitiateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getInitiateList(BaseMethodArgument param, HttpServletRequest request) {
        String sort = getStringParameter(request, "sort");
        String name = getStringParameter(request, "name");
        return BaseRespResult.successResult(currentUserTaskService.getInitiateList(sort, name));
    }

    /**
     * 收藏
     */
    @RequestMapping(value = "/collect", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult collect(BaseMethodArgument param, HttpServletRequest request) {
        String status = getStringParameter(request, "status");
        String piid = getStringParameter(request, "piid");
        String message = "";
        if (status.equals("1")) {
            message = "收藏成功！";
        } else {
            message = "取消收藏成功！";
        }
        return BaseRespResult.successResult(currentUserTaskService.collect(piid, status), message);
    }

    /**
     * 延时
     */
    @RequestMapping(value = "/delay", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult delay(BaseMethodArgument param, HttpServletRequest request) {
        String status = getStringParameter(request, "status");
        String id = getStringParameter(request, "id");
        String message = "";
        if (status.equals("1")) {
            message = "添加延时标识成功！";
        } else {
            message = "取消延时标识成功！";
        }
        return BaseRespResult.successResult(currentUserTaskService.delay(id, status), message);
    }

    /**
     * 获取流程流转历史
     *
     * @return
     */
    @RequestMapping(value = "/getCirculationHistory", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getCirculationHistory(String beanId) {
        return BaseRespResult.successResult(currentUserTaskService.getCirculationHistory(beanId));
    }

    /**
     * 审批告知 置为已读 传id单条已读，不传全部已读
     *
     * @return
     */
    @RequestMapping(value = "/setAsRead", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult setAsRead(BaseMethodArgument param, HttpServletRequest request) {
        String pkId = getStringParameter(request, "pkId");
        return BaseRespResult.successResult(currentUserTaskService.setAsRead(pkId));
    }

    /**
     * 获取流程代办角标
     * @return
     */
    @RequestMapping(value = "/getRreApprovNum", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult getRreApprovNum(BaseMethodArgument param, HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        if(Strings.isNullOrEmpty(userId)||Strings.isNullOrEmpty(deptId)){
            userId = SecurityUserHolder.getCurrentAccountRealUserId();
            deptId = SecurityUserHolder.getCurrentAccountRealOrgId();
        }
        return currentUserTaskService.getRreApprovNum(userId,deptId);
    }
}
