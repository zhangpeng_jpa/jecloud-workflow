package com.je.workflow.job;

import cn.hutool.core.date.StopWatch;
import com.je.bpm.engine.RuntimeService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.DateUtils;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 预警job
 */

@Component
public class EarlyWarningJobImpl implements EarlyWarningJob {

    private static Logger logger = LoggerFactory.getLogger(EarlyWarningJobImpl.class);

    @Autowired
    protected MetaService metaService;
    @Autowired
    private RuntimeService runtimeService;

    @XxlJob("earlyWarningJobHandler")
    @Override
    public void executeJob() {
        logger.info("earlyWarningJobHandler.executeJob task start: " + DateUtils.formatDateTime(new Date()));
        StopWatch watch = new StopWatch();
        watch.start();
        runtimeService.executeEarlyWarningJob();
        watch.stop();
        logger.info("staffStatisticHandler.executeJob task end: " + DateUtils.formatDateTime(new Date()));
        logger.info("staffStatisticHandler.executeJob task 耗时: " + watch.getTotalTimeSeconds() + " 秒");

    }

}
