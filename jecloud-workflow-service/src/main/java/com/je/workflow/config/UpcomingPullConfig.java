/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpcomingPullConfig {

    private static Boolean IS_PULL = false;
    private static String PROD_NAME = null;
    private static String SERVICE_NAME = null;

    public UpcomingPullConfig() {
        init();
    }

    private void init() {
        Config config = ConfigService.getConfig("upcoming");
        String isPull = config.getProperty("upcoming.pull", "0");
        if (isPull.equals("1")) {
            IS_PULL = true;
        }
        PROD_NAME = config.getProperty("upcoming.prodName", "");
        SERVICE_NAME = config.getProperty("upcoming.serviceName", "");
    }

    public static Boolean isPull() {
        return IS_PULL;
    }

    public static String getProdName() {
        return PROD_NAME;
    }

    public static String getServiceName() {
        return SERVICE_NAME;
    }

}
