/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.Objects;

public class ButtonsVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String prod;
    private String tableCode;
    private String funcCode;
    private String beanId;

    private AccountVo account;

    private ButtonsVo() {
    }

    private ButtonsVo(Builder builder) {
        this.prod = builder.prod;
        this.tableCode = builder.tableCode;
        this.funcCode = builder.funcCode;
        this.beanId = builder.beanId;
        this.account = builder.account;
    }

    public static class Builder {
        private String prod;
        private String tableCode;
        private String funcCode;
        private String beanId;
        private AccountVo account;

        public Builder() {
        }

        public ButtonsVo build() {
            if (Strings.isNullOrEmpty(funcCode)) {
                throw new IllegalArgumentException("参数 funcCode 不可以为空");
            }
            if (Strings.isNullOrEmpty(prod)) {
                throw new IllegalArgumentException("参数 prod 不可以为空");
            }
            if (Strings.isNullOrEmpty(beanId)) {
                throw new IllegalArgumentException("参数 beanId 不可以为空");
            }
            if (Strings.isNullOrEmpty(tableCode)) {
                throw new IllegalArgumentException("参数 tableCode 不可以为空");
            }
            if (Objects.isNull(account)) {
                throw new IllegalArgumentException("参数 account 不可以为空");
            }
            return new ButtonsVo(this);
        }

        public Builder prod(String prod) {
            if (Strings.isNullOrEmpty(prod)) {
                throw new IllegalArgumentException("参数 prod 不可以为空");
            }
            this.prod = prod;
            return this;
        }

        public Builder tableCode(String tableCode) {
            if (Strings.isNullOrEmpty(tableCode)) {
                throw new IllegalArgumentException("参数 tableCode 不可以为空");
            }
            this.tableCode = tableCode;
            return this;
        }

        public Builder funcCode(String funcCode) {
            if (Strings.isNullOrEmpty(funcCode)) {
                throw new IllegalArgumentException("参数 funcCode 不可以为空");
            }
            this.funcCode = funcCode;
            return this;
        }

        public Builder beanId(String beanId) {
            if (Strings.isNullOrEmpty(beanId)) {
                throw new IllegalArgumentException("参数 beanId 不可以为空");
            }
            this.beanId = beanId;
            return this;
        }

        public Builder account(AccountVo account) {
            this.account = account;
            return this;
        }
    }

    public String getProd() {
        return prod;
    }

    public String getTableCode() {
        return tableCode;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public String getBeanId() {
        return beanId;
    }

    public AccountVo getAccount() {
        return account;
    }
}
