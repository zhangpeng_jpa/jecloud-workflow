/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

public enum OperatorEnum {

    //----------------------------------------流程----------------------------------------
    /**
     * 流程空启动
     */
    PROCESS_EMPTY_START_OPERATOR("processEmptyStartOperation", "流程空启动操作"),
    /**
     * 流程消息启动
     */
    PROCESS_MESSAGE_START_OPERATOR("processMessageStartOperation", "流程消息启动操作"),
    /**
     * 流程空发起
     */
    PROCESS_EMPTY_SPONSOR_OPERATOR("processEmptySponsorOperation", "流程空发起操作"),
    /**
     * 流程消息发起
     */
    PROCESS_MESSAGE_SPONSOR_OPERATOR("processMessageSponsorOperation", "流程消息发起操作"),
    /**
     * 流程挂起
     */
    PROCESS_HANG_OPERATOR("processHangOperation", "流程挂起操作"),
    /**
     * 流程激活
     */
    PROCESS_ACTIVE_OPERATOR("processActivateOperation", "流程激活操作"),
    /**
     * 流程撤销
     */
    PROCESS_CANCEL_OPERATOR("processCancelOperation", "流程撤销操作"),
    /**
     * 流程作废
     */
    PROCESS_INVALID_OPERATOR("processInvalidOperation", "流程作废操作"),
    //----------------------------------------任务----------------------------------------
    /**
     * 任务提交
     */
    TASK_SUBMIT_OPERATOR("taskSubmitOperation", "任务提交操作"),
    /**
     * 任务委托
     */
    TASK_DELEGATE_OPERATOR("taskDelegateOperation", "任务委托操作"),
    /**
     * 任务取消委托
     */
    TASK_CANCEL_DELEGATE_OPERATOR("taskCancelDelegateOperation", "任务取消委托操作"),
    /**
     * 任务更换负责人
     */
    TASK_CHANGE_ASSIGNEE_OPERATOR("taskChangeAssigneeOperation", "任务更换负责人操作"),
    /**
     * 任务领取
     */
    TASK_CLAIM_OPERATOR("taskClaimOperation", "任务领取操作"),
    /**
     * 任务直送
     */
    TASK_DIRECT_SEND_OPERATOR("taskDirectSendOperation", "任务直送操作"),
    /**
     * 任务驳回
     */
    TASK_DISMISS_OPERATOR("taskDismissOperation", "任务驳回操作"),
    /**
     * 任务退回
     */
    TASK_GOBACK_OPERATOR("taskGobackOperation", "任务退回操作"),
    /**
     * 任务传阅
     */
    TASK_PASSROUND_OPERATOR("taskPassroundOperation", "任务传阅操作"),
    /**
     * 任务传阅已读
     */
    TASK_PASSROUND_READ_OPERATOR("taskPassroundReadOperation", "任务传阅已读操作"),
    /**
     * 任务取回
     */
    TASK_RETREIEVE_OPERATOR("taskRetrieveOperation", "任务取回操作"),
    /**
     * 任务转办
     */
    TASK_TRANSFER_OPERATOR("taskTransferOperation", "任务转办操作"),
    /**
     * 任务催办
     */
    TASK_URGE_OPERATOR("taskUrgeOperation", "任务催办操作"),
    /**
     * 任务通过
     */
    TASK_PASS_OPERATOR("taskPassOperator", "任务通过操作"),
    /**
     * 任务否决
     */
    TASK_VETO_OPERATOR("taskVetoOperator", "任务否决操作"),
    /**
     * 任务弃权
     */
    TASK_ABSTAIN_OPERATOR("taskAbstainOperator", "任务弃权操作"),
    /**
     * 会签-改签
     */
    TASK_REBOOK_OPERATOR("taskRebookOperator", "任务改签操作"),
    /**
     * 会签-人员调整
     */
    TASK_PERSONNEL_ADJUSTMENTS_OPERATOR("taskPersonnelAdjustmentsOperator", "人员调整");

    public static OperatorEnum getOperatorEnumById(String id) {
        if (id.equals(PROCESS_EMPTY_START_OPERATOR.toString())) {
            return PROCESS_EMPTY_START_OPERATOR;
        } else if (id.equals(PROCESS_MESSAGE_START_OPERATOR.toString())) {
            return PROCESS_MESSAGE_START_OPERATOR;
        } else if (id.equals(PROCESS_EMPTY_SPONSOR_OPERATOR.toString())) {
            return PROCESS_EMPTY_SPONSOR_OPERATOR;
        } else if (id.equals(PROCESS_MESSAGE_SPONSOR_OPERATOR.toString())) {
            return PROCESS_MESSAGE_SPONSOR_OPERATOR;
        } else if (id.equals(PROCESS_HANG_OPERATOR.toString())) {
            return PROCESS_HANG_OPERATOR;
        } else if (id.equals(PROCESS_ACTIVE_OPERATOR.toString())) {
            return PROCESS_ACTIVE_OPERATOR;
        } else if (id.equals(PROCESS_CANCEL_OPERATOR.toString())) {
            return PROCESS_CANCEL_OPERATOR;
        } else if (id.equals(PROCESS_INVALID_OPERATOR.toString())) {
            return PROCESS_INVALID_OPERATOR;
        } else if (id.equals(TASK_SUBMIT_OPERATOR.toString())) {
            return TASK_SUBMIT_OPERATOR;
        } else if (id.equals(TASK_DELEGATE_OPERATOR.toString())) {
            return TASK_DELEGATE_OPERATOR;
        } else if (id.equals(TASK_CANCEL_DELEGATE_OPERATOR.toString())) {
            return TASK_CANCEL_DELEGATE_OPERATOR;
        } else if (id.equals(TASK_CHANGE_ASSIGNEE_OPERATOR.toString())) {
            return TASK_CHANGE_ASSIGNEE_OPERATOR;
        } else if (id.equals(TASK_CLAIM_OPERATOR.toString())) {
            return TASK_CLAIM_OPERATOR;
        } else if (id.equals(TASK_DIRECT_SEND_OPERATOR.toString())) {
            return TASK_DIRECT_SEND_OPERATOR;
        } else if (id.equals(TASK_DISMISS_OPERATOR.toString())) {
            return TASK_DISMISS_OPERATOR;
        } else if (id.equals(TASK_GOBACK_OPERATOR.toString())) {
            return TASK_GOBACK_OPERATOR;
        } else if (id.equals(TASK_PASSROUND_OPERATOR.toString())) {
            return TASK_PASSROUND_OPERATOR;
        } else if (id.equals(TASK_PASSROUND_READ_OPERATOR.toString())) {
            return TASK_PASSROUND_READ_OPERATOR;
        } else if (id.equals(TASK_RETREIEVE_OPERATOR.toString())) {
            return TASK_RETREIEVE_OPERATOR;
        } else if (id.equals(TASK_TRANSFER_OPERATOR.toString())) {
            return TASK_TRANSFER_OPERATOR;
        } else if (id.equals(TASK_URGE_OPERATOR.toString())) {
            return TASK_URGE_OPERATOR;
        } else if (id.equals(TASK_PASS_OPERATOR.toString())) {
            return TASK_PASS_OPERATOR;
        } else if (id.equals(TASK_VETO_OPERATOR.toString())) {
            return TASK_VETO_OPERATOR;
        } else if (id.equals(TASK_ABSTAIN_OPERATOR.toString())) {
            return TASK_ABSTAIN_OPERATOR;
        } else if (id.equals(TASK_REBOOK_OPERATOR.toString())) {
            return TASK_REBOOK_OPERATOR;
        }
        return null;
    }

    private String id;
    private String name;

    OperatorEnum(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
